﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


internal class MulticastDelegate
{
    public delegate int MathOperations(int a, int b);

    private MathOperation mathOperation;

    public MulticastDelegate()
    {
        mathOperation = new MathOperation();
    }

    public void Execute()
    {
        MathOperations multicastOperation = null;

        multicastOperation += mathOperation.Add;
        multicastOperation += mathOperation.Subtract;
        
        // Invocar
        foreach (MathOperations del in multicastOperation.GetInvocationList())
        {
            int result = del(2, 1);
            Console.WriteLine(result);
        }
    }
}

