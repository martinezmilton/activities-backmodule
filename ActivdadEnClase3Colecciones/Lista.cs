﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


internal class Lista
{
    public List<int> Integers { get; set; }

    public Lista()
    {
        Integers = new List<int>();
    }

    public void AddIntegers()
    {
        for(int i = 0; i <= 9; i++)
        {
            Integers.Add(i);
        }
    }

    public void PrintList()
    {
        Console.WriteLine("Lista");
        foreach(int num in Integers)
        {
            Console.Write(num);
        }
        Console.WriteLine();
    }

    public void RemoveFirstAndLast()
    {
        if(Integers.Count > 0)
        {
            Integers.Remove(0);
            Integers.Remove(Integers.Count - 1);
        }
        PrintList();
    }
}

