﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


internal class Diccionario
{
    public Dictionary<string, int> students {  get; set; }

    public Diccionario()
    {
        students = new Dictionary<string, int>();
    }

    public void AddStudents()
    {
        students["Milton"] = 90;
        students["Fernando"] = 90;
        students["Juan"] = 85;
        students["Maria"] = 87;
        students["Sofia"] = 93;
    }

    public void PrintStudents()
    {
        Console.WriteLine("Diccionario");
        foreach (var student in students)
        {
            Console.WriteLine(student);
        }
    }



}

