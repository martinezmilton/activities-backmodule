﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;


internal class Delegate
{
    public delegate int MathOperations(int a, int b);

    public MathOperation mathOperation;

    public Delegate()
    {
        mathOperation = new MathOperation();
    }

    public void Execute()
    {
        MathOperations operation = mathOperation.Add;
        int addResult = operation(1, 2); 
        Console.WriteLine(addResult);

        operation = mathOperation.Subtract;
        int subtractResult = operation(2, 1);
        Console.WriteLine(subtractResult);
    }

}

